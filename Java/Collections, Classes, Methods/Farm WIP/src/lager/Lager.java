package lager;

public class Lager {

    private int milk;
    private int eier;
    private int wolle;

    public Lager(int milk, int eier, int wolle) {
        this.milk = milk;
        this.eier = eier;
        this.wolle = wolle;
    }

    public void print() {
        System.out.println("Name: " + milk);
        System.out.println("Gender: " + eier);
        System.out.println("Alter: " + wolle);
    }

    public int getMilk() {
        return milk;
    }

    public void setMilk(int milk) {
        this.milk = milk;
    }

    public int getEier() {
        return eier;
    }

    public void setEier(int eier) {
        this.eier = eier;
    }

    public int getWolle() {
        return wolle;
    }

    public void setWolle(int wolle) {
        this.wolle = wolle;
    }
}
