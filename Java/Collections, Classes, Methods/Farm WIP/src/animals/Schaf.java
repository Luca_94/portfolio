package animals;

public class Schaf extends Animals {

    public Schaf(String name, String gender, int age, int hunger) {
        super(name, gender, age, hunger);
    }

    public void print() {
        System.out.println("Name: " + name);
        System.out.println("Gender: " + gender);
        System.out.println("Alter: " + age);
        System.out.println("Hunger: " + hunger);
    }

}
