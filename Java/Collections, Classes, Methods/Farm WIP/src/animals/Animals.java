package animals;
import java.util.Random;

public abstract class Animals {

    protected String name;
    protected String gender;
    protected int age;
    protected int hunger;


    public Animals(String name, String gender, int age, int hunger) {
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.hunger = hunger;
    }

    public abstract void print();

    public static String animalName() {
        String[] Names = {"Alex", "Andrea", "Hans", "Manfred", "Laura", "Dieter", "Sebastian"};
        Random rand = new Random();
        int randomIndex = rand.nextInt(Names.length);
        return Names[randomIndex];
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHunger() {
        return hunger;
    }

    public void setHunger(int hunger) {
        this.hunger = hunger;
    }
}
