package animals;
import java.util.Random;

public class Kuh extends Animals {


    public Kuh(String name, Gender gender, int age, int hunger) {
        super(NAMES[RANDOM.nextInt(NAMES.length)], String.valueOf(Gender.values()[RANDOM.nextInt(Gender.values().length)]), RANDOM.nextInt(20) + 1, RANDOM.nextInt(20)+1);
    }

    private static final String[] NAMES = {"Daisy", "Bella", "Luna", "Lucy", "Molly"};
    private static final Random RANDOM = new Random();

    public void print() {
        System.out.println("Name: " + name);
        System.out.println("Gender: " + gender);
        System.out.println("Alter: " + age);
        System.out.println("Hunger: " + hunger);
    }
}
