package autohaus;
import fahrzeuge.Fahrzeug;
import java.util.ArrayList;
import java.util.List;

public class Autohaus {

    private List<fahrzeuge.Fahrzeug> fahrzeugList;

    private int anzahlAbstellplaetze;

    private boolean istueberlastet;

    private int mitarbeiter;

    private String name;

    public Autohaus(int id, String brand, int constructed, double basicprice, List<Fahrzeug> fahrzeugList, int anzahlAbstellplaetze, boolean istueberlastet, int mitarbeiter, String name) {
        this.fahrzeugList = new ArrayList<fahrzeuge.Fahrzeug>();
        this.anzahlAbstellplaetze = anzahlAbstellplaetze;
        this.istueberlastet = istueberlastet;
        this.mitarbeiter = mitarbeiter;
        this.name = name;
    }

    public Autohaus() {
        this.fahrzeugList = new ArrayList<fahrzeuge.Fahrzeug>();
    }

    public boolean zuVieleFahrzeuge(boolean istueberlastet) {
        if(mitarbeiter*3 < fahrzeugList.size()){
            return true;
        }  else {
            return false;
        }
    }

    public boolean zuVieleFahrzeuge() {
        if(fahrzeugList.size() > 10){
            return true;
        } else {
            return false;
        }
    }

    public int anzahlFahrzeuge() {
        return fahrzeugList.size();
    }



    public void print(){
        System.out.print("Folgende Marken sind zu verkaufen: ");
        int i = 0;
        for (Fahrzeug mark : getFahrzeugList()) {
            System.out.print(mark.getBrand());
            if (i < getFahrzeugList().size() - 1) {
                System.out.print(", ");
            }
            i++;
        }
        System.out.println();


        for (Fahrzeug price : getFahrzeugList()) {
            System.out.println("Preis von " + price.getBrand() + ": " + price.getPreis());
        }

        for (Fahrzeug type : getFahrzeugList()) {
            if (type instanceof fahrzeuge.PKW) {
                System.out.println("Das Fahrzeug ist ein PKW");
            } else if (type instanceof fahrzeuge.LKW) {
                System.out.println("Das Fahrzeug ist ein LKW");
            }
        }
        System.out.println("Zu wenig Mitarbeiter: " + zuVieleFahrzeuge());
        System.out.println("Mehr als 10 Fahrzeuge: " + zuVieleFahrzeuge());

    }



    public List<Fahrzeug> getFahrzeugList() {
        return fahrzeugList;
    }

    public void setFahrzeugList(String fahrzeugTyp, int id, String marke, int baujahr, int grundpreis, int serviceJahr){
        if (fahrzeugTyp.equals("PKW")) {
            fahrzeuge.PKW pkw = new fahrzeuge.PKW(id, marke, baujahr, grundpreis, serviceJahr);
            fahrzeugList.add(pkw);
        } else if (fahrzeugTyp.equals("LKW")) {
            fahrzeuge.LKW lkw = new fahrzeuge.LKW(id, marke, baujahr, grundpreis);
            fahrzeugList.add(lkw);
        } else {
            System.out.println("Nicht bekannter Fahrzeugtype");
        }
    }

    public int getAnzahlAbstellplaetze() {
        return anzahlAbstellplaetze;
    }

    public void setAnzahlAbstellplaetze(int anzahlAbstellplaetze) {
        this.anzahlAbstellplaetze = anzahlAbstellplaetze;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMitarbeiter() {
        return mitarbeiter;
    }

    public void setMitarbeiter(int mitarbeiter) {
        this.mitarbeiter = mitarbeiter;
    }
}
