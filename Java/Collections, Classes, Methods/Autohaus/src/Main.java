import autohaus.Autohaus;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Autohaus autohaus = new Autohaus();
        autohaus.print();
        Scanner fahrzeuge = new Scanner(System.in);
        String antwort = "j";

        while (true) {
            System.out.println("Fahrzeugtype: ");
            String type = fahrzeuge.nextLine();
            System.out.println("Enter the ID: ");
            int id = fahrzeuge.nextInt();
            fahrzeuge.nextLine();
            System.out.println("Marke: ");
            String marke = fahrzeuge.nextLine();
            System.out.println("Baujahr: ");
            int baujahr = fahrzeuge.nextInt();
            System.out.println("Grundpreis: ");
            int grundpreis = fahrzeuge.nextInt();
            System.out.println("Servicejahr: (-1 if LKW)");
            int servicejahr = fahrzeuge.nextInt();

            autohaus.setFahrzeugList(type, id, marke, baujahr, grundpreis, servicejahr);
            autohaus.print();
            fahrzeuge.nextLine();
            System.out.println("Ein weiteres Fahrzeug hinzufügen? (j/n)");
            antwort = fahrzeuge.nextLine();
            if (antwort.equals("n")) {
                break;
            }
        }
        fahrzeuge.close();
    }
}