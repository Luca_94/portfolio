package fahrzeuge;

public abstract class Fahrzeug {

    protected int id;
    protected String brand;
    protected int constructed;
    protected double basicprice;


    public Fahrzeug(int id, String brand, int constructed, double basicprice) {
        this.id = id;
        this.brand = brand;
        this.constructed = constructed;
        this.basicprice = basicprice;
    }

    public Fahrzeug() {

    }

    public abstract double getRabatt();

    public abstract void print();

    public double getPreis() {
        double basicPriceMinusDiscount = (basicprice / 100) * getRabatt();
        return basicprice - basicPriceMinusDiscount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getConstructed() {
        return constructed;
    }

    public void setConstructed(int constructed) {
        this.constructed = constructed;
    }

    public double getBasicprice() {
        return basicprice;
    }

    public void setBasicprice(double basicprice) {
        this.basicprice = basicprice;
    }
}
