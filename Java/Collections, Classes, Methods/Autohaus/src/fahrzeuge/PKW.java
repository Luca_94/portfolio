package fahrzeuge;
import java.time.Year;

public class PKW extends Fahrzeug{
    private int vorfuehrjahr;
    int year = Year.now().getValue();
    private int vorfuehrWagenJahr;


    public PKW(int id, String brand, int constructed, double basicprice, int vorfuehrWagenJahr) {
        super(id, brand, constructed, basicprice);
        this.vorfuehrWagenJahr = vorfuehrWagenJahr;
    }

    @Override
    public double getRabatt(){
        double carAge = year - constructed;
        int discount = 5;
        int jahr = year - vorfuehrjahr;
        double percentage = (discount + jahr) * carAge;
        if(percentage > 10){
            return 10;
        } else return percentage;
    }

    public String setVorfuehrWagenJahr(int jahr) {
        if (vorfuehrjahr >= 1890 && vorfuehrjahr <= year) {
            jahr = vorfuehrjahr;
            vorfuehrWagenJahr = jahr;
            return "Zulässig";
        } else {
            return "Nicht zulässig";
        }
    }

    @Override
    public void print(){
        System.out.print("ID: " + id + "\n");
        System.out.print("Marke: " + brand + "\n");
        System.out.println("Baujahr: " + constructed);
        System.out.println("Preis: " + basicprice);
        System.out.println("Vorfuehrjahre: " + vorfuehrjahr);
        System.out.println("Rabatt: " + getRabatt());
        System.out.println("Neuer Preis: " + getPreis());
        System.out.println("Letztes Vorführjahr: " + setVorfuehrWagenJahr(vorfuehrWagenJahr));
    }

    /*-----------------------getter & setter_____________________*/
    public int getVorfuehrjahr() {
        return vorfuehrjahr;
    }

    public void setVorfuehrjahr(int vorfuehrjahr) {
        this.vorfuehrjahr = vorfuehrjahr;
    }
}
