package fahrzeuge;
import java.time.*;

public class LKW extends Fahrzeug {
    int year = Year.now().getValue();

    public LKW(int id, String brand, int constructed, double basicprice) {
        super(id, brand, constructed, basicprice);
    }

    @Override
    public double getRabatt(){
        double carAge = year - constructed;
        int discount = 6;
        double percentage = discount * carAge;
        if(percentage > 15){
            return 15;
        } else return percentage;
    }

    @Override
    public void print() {
        System.out.print("ID: " + id + "\n");
        System.out.print("Marke: " + brand + "\n");
        System.out.println("Baujahr: " + constructed);
        System.out.println("Preis: " + basicprice);
        System.out.println("Rabatt: " + getRabatt());
        System.out.println("Neuer Preis: " + getPreis());
    }}