const addPersonForm = document.getElementById('addPersonForm');
const addPersonFirstName = document.getElementById('addPersonFirstName');
const addPersonLastName = document.getElementById('addPersonLastName');
const addPersonBirthYear = document.getElementById('addPersonBirthYear');
const addPersonPassWord = document.getElementById("addPersonPassWord");
const addPersonUserName = document.getElementById("addPersonUserName");
const formBtnCollapse = document.getElementById("formButton");
const submitButton = document.getElementById("submitButton");
const resetButton = document.getElementById("clearAll");
const forgotPassword = document.getElementById("forgotPassword");
const navLogo = document.getElementById("navLogo");
const birthcounter = document.getElementById("birthCounter");
const changePassword = document.getElementById("changePasswordButton");
const changePasswordInput = document.getElementById("changePassword");
const modal = document.getElementById('passwordModal');



let persons = [
    createPersonObject('Lucas', 'Müller', 1994, 'abc123', 'Luca'),
    createPersonObject('Hans', 'Haller', 1991, '123abc', 'User1'),
    createPersonObject('Manfred', 'Knaller', 1971, '333232', 'User2'),
    createPersonObject('Alex', 'Luller', 1968, 'dsd3hd3h3d', 'User3'),
    createPersonObject('Max', 'Mustermann', 1983, 'pass123', 'User 4'),
    createPersonObject('Andrea', 'Henker', 1998, 'pass321', 'User 5'),
];

window.addEventListener("load", checkLoginStatus);
let loginContainer = document.getElementById("loginContainer");

function displayLoginForm() {
    loginContainer.innerHTML = `
    <input type="text" id="passWordInput" placeholder="Right Field">
    <p>Username in the "Left Field" <br><span>
        <button class="btn btn-warning" style="color: rgba(255, 217, 0, 0.456);" id="loginbtn">Login</button></span>
        <br> 
        Password in the "Right Field"</p>
    <input type="password" id="userNameInput" placeholder="Left Field">`
    const loginBtn = document.getElementById("loginbtn");
    loginBtn.addEventListener('click', handleLogin);
};

function displayLogoutForm() {
    loginContainer.innerHTML = `<button type="click" class="btn btn-danger" id="logOutButton">Logout</a>`;
    let logoutBtn = document.getElementById("logOutButton");
    logoutBtn.addEventListener('click', handleLogout);
};

function handleLogin(event) {
    event.preventDefault();
    let userInput = document.getElementById("userNameInput").value;
    let passInput = document.getElementById("passWordInput").value;
    let userFound = persons.find(user => user.userName == userInput && user.passWord == passInput);
    localStorage.setItem("userLogged", userFound.userName);
    localStorage.setItem("passLogged", userFound.passWord);

    if (userFound) {
        let confirmAge = prompt("Input your age:");
        if (confirmAge == userFound.age) {
            let confirmMath = prompt("Input half of your age:");
            if (confirmMath == userFound.age / 2) {
                let confirmMath2 = prompt("Year of Birth 'minus' age:");
                if (confirmMath2 == userFound.birthYear - userFound.age) {
                    let lastConfirm = prompt("Last but not least, the password:");
                    if (lastConfirm == userFound.passWord) {
                        localStorage.setItem("loggedin", "true");
                        checkLoginStatus();
                    }
                }
            }
        }
    } else {
        localStorage.setItem("loggedin", "false");
    }
};

function checkLoginStatus() {
    let loggedin = localStorage.getItem("loggedin");
    if (loggedin === "true") {
        displayLogoutForm();
    } else {
        displayLoginForm();
    }
};

function handleLogout() {
    localStorage.setItem("loggedin", "false");
    checkLoginStatus();
};

navLogo.addEventListener("click", event => 
event.preventDefault()
);

changePasswordInput?.addEventListener("input", () => {
    const modalRandom = (min, max) => Math.floor(Math.random()*(max-min+1)+min);
    modal.style.left = modalRandom(0, 600 -100)+'px';
    modal.style.top = modalRandom(0, 600 -100)+'px';
    document.getElementById("changePassword").blur();
    });


changePassword?.addEventListener("click", event => {
    event.preventDefault();
    let newPassword = document.getElementById("changePassword").value;
    let personToChange = localStorage.getItem('userLogged');
    localStorage.setItem('passLogged', newPassword);
    let index = persons.findIndex(person => person.userName === personToChange);
    if (index !== -1) {
    persons[index].passWord = newPassword;
    document.getElementById("changePassword").value = "";
    }
    });

forgotPassword.addEventListener("click", event => {
    event.preventDefault();
    const userInput = document.getElementById("userNameInput");
    const passInput = document.getElementById("passWordInput")
    const select = document.createElement("select");
    const select2 = document.createElement("select");
    select.id = "userNameInput";
    select2.id = "passWordInput"

    for (let i = persons.length -1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i +1));
        [persons[i], persons[j]] = [persons[j], persons[i]];
    }

    persons.forEach(persons => {
        const option = document.createElement("option");
        const option1 = document.createElement("option");
        option.value = persons.userName;
        option1.value = persons.passWord;
        option.text = persons.userName;
        option1.text = persons.passWord
        select.appendChild(option);
        select2.appendChild(option1);
    });
    userInput.replaceWith(select);
    passInput.replaceWith(select2);
});

function drag(e) {
    formBtnCollapse.style.position = "absolute";
    formBtnCollapse.style.left = e.pageX-30+"px";
    formBtnCollapse.style.top = e.pageY-30+"px";
    addPersonForm.style.position = "absolute";
    addPersonForm.style.left = e.pageX-30+"px";
    addPersonForm.style.top = e.pageY-5+"px";
}

formBtnCollapse?.addEventListener("click", () =>
    document.addEventListener("mousemove", drag)
);
formBtnCollapse?.addEventListener("contextmenu", event => {
    event.preventDefault();
document.removeEventListener("mousemove", drag)
});
addPersonForm?.addEventListener("contextmenu", event => {
    event.preventDefault();
    document.removeEventListener("mousemove", drag)
});



resetButton?.addEventListener("click", event => {
    event.preventDefault();
    addPersonFirstName.value = '';
    addPersonLastName.value = '';
    addPersonBirthYear.value = '';
    addPersonPassWord.value = '';
    addPersonUserName.value = '';
});


let currentIndex = 0;

document.querySelector("#birthCounter")?.addEventListener("click", event => {
  event.preventDefault();
  if (currentIndex >= 4) {
    return;
  }
  let value = document.querySelector("#addPersonBirthYear").value;
  let currentValue = parseInt(value[currentIndex], 10);
  if (isNaN(currentValue)) {
    currentValue = 0;
  }
  currentValue = (currentValue + 1) % 10;
  value = value.substr(0, currentIndex) + currentValue + value.substr(currentIndex + 1);
  document.querySelector("#addPersonBirthYear").value = value;
});

document.querySelector("#nextChar")?.addEventListener("click", event => {
    event.preventDefault();
    if (currentIndex >= 3) {
        currentIndex = 0;
      } else {
        currentIndex += 1;
      }
    let value = document.querySelector("#addPersonBirthYear").value;
    if (value.length >= 4) {
        return;
    }
    value = value.padEnd(value.length + 1, '0');
    document.querySelector("#addPersonBirthYear").value = value;
  });


addPersonForm?.addEventListener('submit', event => {
    event.preventDefault();

    const firstName = addPersonFirstName.value;
    const lastName = addPersonLastName.value;
    const birthYear = parseInt(addPersonBirthYear.value);
    const passWord = addPersonPassWord.value;
    const userName = addPersonUserName.value;

    let checkUserName = persons.findIndex(persons => persons.userName === userName);

    if (checkUserName !== -1) {
        alert("Username existiert bereits");
    } else {

    let confirm1 = confirm("Are you sure you want to add this User?");
    if (confirm1) {
        let confirm2 = confirm("Are you really sure?");
        if (confirm2) {
            let confirm3 = prompt("Just to make sure, confirm the password")
            if (confirm3 == passWord) {
                let confirm4 = prompt
                (`Is this your Firstname?
                ===>${firstName}<===
                Just enter it again to check`)
                if (confirm4 == firstName) {
                    persons.push(createPersonObject(firstName, lastName, birthYear, passWord, userName));
                }
            }
        }
    } else {
    };

    addPersonFirstName.value = '';
    addPersonLastName.value = '';
    addPersonBirthYear.value = '';
    addPersonPassWord.value = '';
    addPersonUserName.value = '';
}});


function createPersonObject(firstName, lastName, birthYear, passWord, userName) {
    return {
        firstName,
        lastName,
        birthYear,
        passWord,
        userName,
        get age() {
            return new Date().getFullYear() - this.birthYear
        },
        set age(age) {
            this.birthYear = new Date().getFullYear() - age
        }
    };
};


const diceButton = document.querySelector("#roll-dice");
const cardContainer = document.querySelector(".card-container");
const cards = document.querySelectorAll(".card");
let counterDice = document.getElementById("die");

diceButton?.addEventListener("click", () => {
const randomNumber = Math.floor(Math.random() * 6) + 1;
counterDice.innerText = randomNumber;

cards.forEach(card => {
card.style.display = "none";
});

cards[randomNumber - 1].style.display = "block";
});


