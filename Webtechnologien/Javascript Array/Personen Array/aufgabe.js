    const personTableBody = document.getElementById('personTableBody');
    const contextMenuElement = document.getElementById('contextMenu');
    personTableBody.addEventListener("contextmenu", event => {
        event.preventDefault();
        contextMenuElement.classList.add("show");
        contextMenuElement.style.top = event.pageY + "px";
        contextMenuElement.style.right = (window.innerWidth - event.pageX) + "px";
        handleMenuOpen(event);
    });

    document.addEventListener("click", event => {
        if (event.target !== contextMenuElement && !contextMenuElement.contains(event.target)) {
            contextMenuElement.classList.remove("show");
        }
    });

    const deletePersonButton = document.getElementById('deletePerson');
    deletePersonButton.addEventListener("click", () => {
        persons.splice(selectedPersonIndex, 1);
        contextMenuElement.classList.remove("show");
        render()
    });

    const changeAgeButton = document.getElementById('changeAge');
    changeAgeButton.addEventListener("click", event => {
        const personToChange = persons[selectedPersonIndex];
        const newAge = prompt(`Geben Sie das neue Alter für ${personToChange.firstName} ${personToChange.lastName} ein:`);
        if (newAge) {
            personToChange.age = newAge;
            contextMenuElement.classList.remove("show");
            render();
        }
    })

    let selectedPersonIndex = null;
    function handleMenuOpen(event) {
        const elements = event.composedPath();
        const row = elements.find(element => element.tagName === "TR");
        console.log(row);
        const personIndex = row.dataset.index;
        selectedPersonIndex = personIndex;
    };

    
    const addPersonForm = document.getElementById('addPersonForm');
    const addPersonFirstName = document.getElementById('addPersonFirstName');
    const addPersonLastName = document.getElementById('addPersonLastName');
    const addPersonBirthYear = document.getElementById('addPersonBirthYear');
    const clearAllButton = document.getElementById('clearAllButton');

    let persons = [
        createPersonObject('Maria', 'Huber', 1990),
        createPersonObject('Franz', 'Müller', 1978),
        createPersonObject('Gerhard', 'Gruber', 1991),
        createPersonObject('Alina', 'Steiner', 1997)
    ];
    render();

    addPersonForm.addEventListener('submit', event => {
        event.preventDefault();

        const firstName = addPersonFirstName.value;
        const lastName = addPersonLastName.value;
        const birthYear = parseInt(addPersonBirthYear.value);

        persons.push(createPersonObject(firstName, lastName, birthYear));

        addPersonFirstName.value = '';
        addPersonLastName.value = '';
        addPersonBirthYear.value = '';

        render();
    });

    clearAllButton.addEventListener('click', () => {
        persons = [];
        render();
    });

    function createPersonObject(firstName, lastName, birthYear) {
        return {
            firstName,
            lastName,
            birthYear,
            get age() {
                return new Date().getFullYear() - this.birthYear
            },
            set age(age) {
                this.birthYear = new Date().getFullYear() - age
            }
        };
    }

    function render() {
        personTableBody.innerHTML = '';
        persons.forEach((person, index) => {
            let personTableRow = `<tr data-index="${index}">`;
            personTableRow += `<td>${person.firstName}</td>`;
            personTableRow += `<td>${person.lastName}</td>`;
            personTableRow += `<td>${person.age} (<span class="bi-calendar-heart"></span> ${person.birthYear})</td>`;
            personTableRow += '</tr>';
            personTableBody.innerHTML += personTableRow;
        });
    }